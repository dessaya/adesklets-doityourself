#!/usr/bin/env python

#
# This script prints the date and subject of the last mails received in your
# inbox.
#

#
# Configuration section
#

import os

# Mailbox file name (or directory for maildir-style folders)
filename = os.environ['HOME'] + '/mail/inbox'

# Mailbox format, it can be "mbox" for traditional UNIX mailboxes, "maildir"
# for qmail's maildir, "mmdf" for MMDF style and "mh" for MH mailboxes.
format = 'mbox'

# Number of messages to show
show = 5


#
# Script code - Do NOT touch past here unless you know what you're doing.
#

import sys
import mailbox
import email



def msgfactory(fp):
	# from the mailbox module documentation example
	try:
		return email.message_from_file(fp)
	except email.Errors.MessageParseError:
		# Don't return None since that will
		# stop the mailbox iterator
		return ''

def getmbox():
	if format == 'mbox':
		reader = mailbox.UnixMailbox
	elif format == 'maildir':
		reader = mailbox.Maildir
	elif format == 'mmdf':
		reader = mailbox.MmdfMailbox
	elif format == 'mh':
		reader = mailbox.MHMailbox
	else:
		print 'Error! Unknown mailbox format'
		sys.exit(1)

	return reader(open(filename), msgfactory)

def getlast(mbox, last):
	msgs = [None for i in range(last)]
	n = 0
	for m in mbox:
		msgs[n] = m
		n = (n + 1) % last
	return msgs[n:] + msgs[:n]

def printmsgs(msgs):
	for m in msgs:
		print m['date'], email.Utils.parseaddr(m['from'])[0]
		print '\t', m['subject']


if __name__ == '__main__':
	mbox = getmbox()
	msgs = getlast(mbox, show)
	printmsgs(msgs)


