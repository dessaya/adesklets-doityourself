#!/bin/bash

# This script gives information about the frequency of your CPU(s), if your
# hardware supports clock scaling.

# CPU IDs to watch; if you have only one set it to "0"; if you have two
# without HT use "0 1", two with HT "0 2"; and so on, you get the idea.
CPUS="0"

# Exit immediately if any command fails:
set -e

function print_cpu_info {
	F=`cat /sys/devices/system/cpu/cpu$1/cpufreq/scaling_cur_freq`
	F=`echo "scale = 2; $F / 1000 / 1000" | bc`
	M=`cat /sys/devices/system/cpu/cpu$1/cpufreq/scaling_max_freq`
	M=`echo "scale = 2; $M / 1000 / 1000" | bc`
	echo "CPU $1: ${F} GHz / ${M} GHz"
}

case $1 in
	init)
		echo "<background.color None/>"
		;;
	test | click)
		for i in $CPUS; do
			print_cpu_info $i
		done
		;;
	*)
		echo "Usage: $0 [ init | test | click ] test-value"
esac

