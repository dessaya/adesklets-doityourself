#!/bin/bash

# This is a sample back-end script for doityourself. It shows your To Do List,
# which is taken from a text file. On click, it launches the editor on the
# To Do file.

# Exit immediately if any command fails:
set -e

export EDITOR=${EDITOR:-vi}
TODO="$HOME/.todo"

case $1 in
	init)
		cat <<-EOF
		<window.never_shrink False/>
		<background.color None/>
		<test_delay 3600/>
		EOF
		exit 0
		;;
	test)
		# Test if the TODO file exists:
		if [ ! -r $TODO ]; then
			cat <<-EOF
			<color 'FF0000'/>
			<col/><image 'error.png' 48 48/><endcol/><col/>
			This desklet shows your To Do List.
			The list is taken from the file $TODO.
			Click to create the To Do file.
			EOF
			exit 0
		fi
		;;
	click)
		# launch editor
		xterm -e $EDITOR $TODO
		;;
esac

# Show the TODO file:
cat <<-EOF
<font 'Vera/14'/><color 'FF0000'/>
To Do List
<hline 300/>
<font 'Vera/10'/><color 'FFFFFF'/>
EOF
cat $TODO

