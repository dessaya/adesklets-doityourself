#!/bin/bash

#
# This script is an interface to xrandr(1x), and allows to rotate the
# screen with one click. Of course, this is useless if your monitor can't
# be rotated :)
#

# Exit immediately if any command fails:
set -e

function rot_normal()
{
	xrandr -q | grep rotation | grep -q normal
}

function set_icon()
{
	if rot_normal; then
		echo "<button 'monitor1.png'/>"
	else
		echo "<button 'monitor2.png'/>"
	fi
}

case $1 in
	init)
		echo "<test_delay 600/>"
		echo "<background.color None/>"
		;;
	test)
		set_icon
		;;
	click*)
		if rot_normal; then
			xrandr -o left
		else
			xrandr -o normal
		fi
		sleep 2
		set_icon
		;;
	*)
		echo "Usage: $0 [ init | test | click ] test-value"
esac

