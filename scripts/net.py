#!/usr/bin/env python

#
# Configuration
#

# Time between updates
delay = 3

# Interfaces to watch for
monitor = ['eth0']
#monitor = ['ppp0', 'ppp1']

# For each interface you define here, two bars will be displayed for each
# listed interface (one for tx and one for rx). If you omit it, you'll just
# see the rates.
maxs = {
	# 'ifname': (max_rx, max_tx)
	# both numbers in kb/s

	'eth0': (128, 10),
	#'ppp0': (55, 13),
	#'ppp1': (55, 13),
}

# In case you choose to use the bar, if you set this variable to True it will
# use logarithmic scale for the bar (if set to False, it will use a normal
# scale). If you don't use the bar (nothing in maxs), it's not used.
log_bar = True

# Size of the bars, in case you use them. The default size is set to look nice
# with the default font; so if you change the font and want to tune the bar,
# this is the place you're looking for. It's (width, height)
bar_size = (60, 10)


#
# Real code - Do NOT touch past here unless you know what you're doing.
#

import sys
import string


class IfData:
	def __init__(self):
		self.ifname = ''
		self.rxbytes = 0
		self.rxpkts = 0
		self.txbytes = 0
		self.txpkts = 0

	def __repr__(self):
		s = '%s %d %d %d %d' % (self.ifname, self.rxbytes,
				self.rxpkts, self.txbytes, self.txpkts)
		return s

	def fromline(self, line):
		line = line.split()
		t = line[0].split(':')
		if not t[1]:
			t = [t[0], ]
		line = t + line[1:]

		self.ifname = line[0]
		self.rxbytes = int(line[1])
		self.rxpkts = int(line[2])
		self.txbytes = int(line[9])
		self.txpkts = int(line[10])

	def fromtuple(self, t):
		self.ifname = t[0]
		self.rxbytes = int(t[1])
		self.rxpkts = int(t[2])
		self.txbytes = int(t[3])
		self.txpkts = int(t[4])

	def totuple(self):
		return (self.ifname, self.rxbytes, self.rxpkts, self.txbytes,
				self.txpkts)


def get_procdata():
	data = {}
	f = open('/proc/net/dev')
	for line in f:
		line = line.strip()
		if not line:
			continue
		if ':' not in line:
			continue
		idata = IfData()
		idata.fromline(line)
		data[idata.ifname] = idata
	return data

def get_strdata(s):
	data = {}
	s = s.split('/')
	for i in s:
		# we get the string surrounded by "'", remove them before
		# parsing
		i = i.replace("'", '')
		t = i.split()
		if not t or len(t) != 5:
			continue
		idata = IfData()
		idata.fromtuple(t)
		data[idata.ifname] = idata
	return data

def dump_data(data):
	l = [str(data[ifname]) for ifname in data]
	s = string.join(l, '/')
	return s

def get_delta(s1, s2, t):
	drb = (s2.rxbytes - s1.rxbytes) / float(t)
	drp = (s2.rxpkts - s1.rxpkts) / float(t)
	dtb = (s2.txbytes - s1.txbytes) / float(t)
	dtp = (s2.txpkts - s1.txpkts) / float(t)

	return (drb, drp, dtb, dtp)

def get_data_delta(data1, data2, t):
	deltas = {}
	for ifname in data1:
		deltas[ifname] = get_delta(data1[ifname], data2[ifname], t)
	return deltas


def main():
	if len(sys.argv) < 3:
		# no previous data, get it and just save, without any output
		d = get_procdata()
		s = dump_data(d)
		print "<test_value '%s' />" % s
		return

	if sys.argv[1] == 'init':
		print "<test_delay %d/>" % delay
		print "<background.color None/>"
		print "<halign 'center'/>"
		print "<hpadding 10/>"
		#print "<debug True/>"
		if log_bar:
			print "<bar_meter.logarithmic True/>"
		return

	prevdata = get_strdata(sys.argv[2])
	curdata = get_procdata()
	d = get_data_delta(prevdata, curdata, delay)

	for ifname in monitor:
		rx = d[ifname][0] / 1024
		tx = d[ifname][2] / 1024
		print '%s:\\' % (ifname)
		# Rx:
		print '<col/>'
		print 'rx: %.2f' % (rx)
		if ifname in maxs:
			# show the bar
			print "<bar_meter.color '00FF0064'/>"
			print "<bar_meter %d %d %.2f %.2f/>" % \
				(bar_size[0], bar_size[1], rx, maxs[ifname][0])
		print '<endcol/>\\'
		# Tx:
		print '<col/>'
		print 'tx: %.2f' % (tx)
		if ifname in maxs:
			# show the bar
			print "<bar_meter.color 'FF000064'/>"
			print "<bar_meter %d %d %.2f %.2f/>" % \
				(bar_size[0], bar_size[1], tx, maxs[ifname][1])
		print '<endcol/>\\'

	print "<test_value '%s' />" % dump_data(curdata)


if __name__ == '__main__':
	try:
		main()
	except:
		raise
		#print 'Argh!'; return
		# for debugging purposes
		print 'There is an error in the script!'
		print 'Please report!'
		print' Error information:'
		print
		import traceback
		s = traceback.format_exc()
		print s


