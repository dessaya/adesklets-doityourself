#!/bin/bash

# This is a back-end script for doityourself. It enables to start and stop
# MLdonkey, and shows some UP/DL stats. Requires netcat.

export MLDONKEY_DIR=${MLDONKEY_DIR:-$HOME/.mldonkey}

function test_pid()
{
	local PID=`pgrep mlnet`
	if [ $PID ]; then
		echo "<color 'FFFFFF'/>"
		echo "<col/><button 'mldonkey-on.png'/><endcol/><col/>"
		return $PID
	else
		echo "<color 'A0A0A0'/>"
		echo "<col/><button 'mldonkey-off.png'/><endcol/><col/>"
		return 0
	fi
}

case $1 in
	init)
		cat <<-EOF
		<background.border.color 'FFFFFF64'/>
		<background.border.padding 5/>
		<window.click_anywhere False/>
		<test_delay 10/>
		<vpadding 10/>
		<hpadding 10/>
		EOF
		;;
	test)
		if test_pid; then
			cat <<-EOF
			
			MLdonkey is not running.
			
			<test_value 0/>
			EOF
		else
			set -e
			# Show UP/DL stats:
			{ echo vd; echo q; } | nc localhost 4000 | tail +9 | head -n 1 | \
			sed 's/ | /\n/g'
			echo "<test_value 1/>"
		fi
		;;
	click_widget)
		set -e
		if [ $3 == '0' ]; then
			# Start MLdonkey:
			cd $MLDONKEY_DIR
			mlnet -daemon >/dev/null 2>&1
		else
			# Stop MLdonkey:
			echo kill | nc localhost 4000 >/dev/null
		fi
		set +e

		if test_pid; then
			cat <<-EOF
			
			MLDonkey stopped.
			
			<test_value 0/>
			EOF
		else
			cat <<-EOF
			
			MLDonkey started.
			
			<test_value 1/>
			EOF
		fi
		;;
	*)
		echo "Usage: $0 [ init | test | click ] test-value"
esac

