#!/bin/bash

# This is a sample back-end script for doityourself. 
# See README if you want to write your own script.

# Exit immediately if any command fails:
set -e

case $1 in
	init)
		# Initialize context variables:
		cat <<-EOF
		<test_delay 2/>
		<bar_meter.color 'FFFFFF64'/>
		EOF
		;;
	test)
		# Define some colors:
		BRIGHT="'FFFFFF'"
		NORMAL="'C0C0C0'"

		# A random value for the bar meter:
		VALUE=$(( $RANDOM %100 ))

		cat <<-EOF
		<col/><image 'dys-color.png' 48 48/><endcol/><col/>
		<font 'Vera/14'/><color $BRIGHT/>
		Example script
		<hline -1/>
		<font 'Vera/10'/><color $NORMAL/>
		This is the output from \\
		<font 'VeraMono/10'/><color $BRIGHT/>example.sh
		<font 'Vera/10'/><color $NORMAL/>
		You should replace it with your own script.
		A random bar meter: <bar_meter -1 10 $VALUE 100/>
		EOF
		;;
	*)
		echo "Usage: $0 [ init | test ] test-value"
		;;
esac

