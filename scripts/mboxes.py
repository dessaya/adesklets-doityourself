#!/usr/bin/env python

#
# This script is another mail checker. It works only on mbox type boxes, and
# is useful if you have procmail or similar filter your mail in several mboxes.
# It shows the boxes with unread mail. On click, opens the box with new mail
# in mutt. Sorry, only mutt is supported :P
#

#
# Configuration section
#

from os import environ

# Directory where the mboxes are stored:
mail_dir = environ['HOME'] + '/mail'

# Boxes to check:
boxes = ['inbox', 'work', 'lug']

# Command to run when new mail arrives; set to None to disable:
command_on_new = '/usr/bin/beep'

#
# Script code - Do NOT touch past here unless you know what you're doing.
#
import sys

if sys.argv[1] == 'init':
	thischeck = {}
	for mbox in boxes:
		thischeck[mbox] = {'size': 0, 'mtime': 0, 'new': 0}
	print '<test_value '+ `thischeck` +'/>'
	print '<background.color None/>'
	print '<window.never_shrink False/>'
	print '<test_delay 10/>'
	print '<autoclear False/>'
	sys.exit()

from os import spawnlp, P_NOWAIT
from os.path import join


lastcheck = eval(sys.argv.pop())

if 'click' in sys.argv[1]:
	cmd = ['aterm', '-e', 'mutt']
	boxes_with_new_mail = [mbox for mbox in boxes if lastcheck[mbox]['new']]
	if boxes_with_new_mail:
		cmd += ['-f', '=' + boxes_with_new_mail[0]]
	else:
		cmd += ['-y']
	spawnlp(P_NOWAIT, cmd[0], *cmd)
	sys.exit()

import mailbox
from os import stat, utime

thischeck = {}
runcmd = False

for mbox in boxes:
	thischeck[mbox] = {}
	# Get mbox inode properties
	#
	mboxpath = join(mail_dir, mbox)
	s = stat(mboxpath)
	if (s.st_size == lastcheck[mbox]['size'] and
		s.st_mtime == lastcheck[mbox]['mtime']):
		# mbox has not changed on disk
		thischeck[mbox] = lastcheck[mbox]
	else:
		# mbox has changed
		thischeck[mbox]['new'] = 0
		for m in mailbox.PortableUnixMailbox(file(mboxpath)):
			if m.get('status','N').find('N') != -1:
				thischeck[mbox]['new'] += 1

		# If new messages, run command:
		if thischeck[mbox]['new'] > lastcheck[mbox]['new']:
			runcmd = True

		# Restore modification & access times:
		utime(mboxpath, (s.st_atime, s.st_mtime))

		# Remember size and time
		thischeck[mbox]['size'] = s.st_size
		thischeck[mbox]['mtime'] = s.st_mtime

if runcmd and command_on_new:
	spawnlp(P_NOWAIT, command_on_new.split()[0], *command_on_new.split())

boxes_with_new_mail = [mbox for mbox in boxes if thischeck[mbox]['new']]

print '<clear/>'

if boxes_with_new_mail:
	print '<button "email.png" 24 24/>\\'
	print ', '.join([mbox + ' (' + `thischeck[mbox]['new']` + ')' for mbox in \
	                 boxes_with_new_mail])
else:
	print '<button "noemail.png" 24 24/>'

print '<test_value '+ `thischeck` +'/>'
