#!/bin/bash

#
# Show lmsensors info.
#

# Exit immediately if any command fails:
set -e

case $1 in
	init)
		echo "<background.color None/>"
		;;
	test | click)
		SOUT=`sensors`

		FAN=`echo "$SOUT" | grep "CPU_Fan:" | sed s/'\s\+'/' '/g | cut -d ' ' -f 2,3`
		CPU=`echo "$SOUT" | grep "CPU:" | sed s/'\s\+'/' '/g | cut -d ' ' -f 2 | sed s/+//g | cut -d '�' -f 1`
		CHASSIS=`echo "$SOUT" | grep "Board:" | sed s/'\s\+'/' '/g | cut -d ' ' -f 2 | sed s/+//g | cut -d '�' -f 1`

		echo "Fan: <hspace -1/>$FAN"
		echo "CPU: <hspace -1/>$CPU C"
		echo "Chassis: <hspace -1/>$CHASSIS C"

		;;
	*)
		echo "Usage: $0 [ init | test | click ] test-value"
esac

