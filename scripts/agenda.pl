#!/usr/bin/perl

# This is a back-end script for doityourself. It parses the Evolution calendar
# and addressbook and shows the dates and birthdays that come in the following
# days. Launches Evolution on click.
#
# Edit the configuration variables below to adapt it to your preferences.
# 
# Disclaimer: I wrote this script long before doityourself. It is written in
# rough Perl, and I didn't mean it to be legible at all. It is not even in
# English. And it does not even work perfectly :P
# So, if you want to modify it to your needs, maybe you are best off
# rewriting it. You are warned.

use strict;
use warnings;

if (not scalar(@ARGV))
{
	print STDERR "Usage: agenda.pl [init|test|click]\n";
	exit 1;
}

if ($ARGV[0] eq 'init')
{
	print "<window.never_shrink False/>";
	print "<test_delay 3600/>";
	print "<background.color None/>";
	exit 0;
}
elsif ($ARGV[0] eq 'click')
{
	system "evolution &";
}

#============================ Configuration ==================================
# Location of Evolution's address book and Calendar:
use constant CAL_FILE =>
		"$ENV{HOME}/.evolution/calendar/local/system/calendar.ics";
use constant AB_FILE =>
		"$ENV{HOME}/.evolution/addressbook/local/system/addressbook.db";

# Show dates & birthdays that fall between these dates:
my $min_diff = - 1 * 24 * 3600; # 1 day before now
my $max_diff = + 7 * 24 * 3600; # 1 week from now

# Uncomment if you want weekdays in your language:
#use POSIX 'locale_h';
#setlocale(LC_ALL, "");
#=============================================================================

use Time::Local;
use POSIX 'strftime';

use DB_File;

my $ahora = time;

my %citas = ();

sub get_attrs
{
	my ($key) = @_;
	my %ret;
	
	#die if $key !~ /[^:]+:(.+)/;
	my @props = split /;/, $key;
	for (@props) { 
		die if not /([^=]+)=(.+)/;
		$ret{$1} = $2;
	}
	return %ret;
}

sub findkey
{
	my ($key, $hash) = @_;

	my $regexp = "^".$key;
	for (keys %{$hash}) {
		if (/$regexp/) { return $_  } 
	}
	return "";
}

sub get_line
{
	my ($fh) = @_;

	my $buf;
	my $line;
	my $pos;

	while (not eof $fh)
	{	
		$buf = <$fh>;
		chomp $buf;
		if (! $line) { 
			$line = $buf; 
			$pos = tell $fh;
		}
		else
		{
			if ($buf =~ /^ (.*)/)
			{
				$line .= $1;
				$pos = tell $fh;
			}
			else
			{
				seek $fh, $pos, 0;
				return $line;
			}
		}
	}

	return 0;
}

use Encode;

sub weekday()
{
	my ($time, $now, ) = @_;
	my $day = (localtime($time))[7];
	my $today = (localtime($now))[7];
	my $weekday = "";
	if ($day == $today)
	{
		$weekday = "<color 'FF0000'/>Today";
	}
	elsif (($day == $today + 1) or ($day == 0 and $today >= 364))
	{
		$weekday = "<color 'FF8080'/>Tomorrow";
	}
	else
	{
		$weekday = "<color 'FFFFFF'/>".ucfirst strftime("%A %e", localtime $time);
	}
	return $weekday;
}

sub parse_cal()
{
	open my $fh, "< ".CAL_FILE or die CAL_FILE.": $!";

	while (not eof $fh)
	{
		while (<$fh>) { chomp; last if /^BEGIN:VEVENT/ }

		my %prop;

		while (not eof $fh)
		{
			$_ = &get_line($fh);
			last if /^END:VEVENT/;

			next if not /^([^:]+):(.*)/;
			$prop{$1} = $2;
		}

		my $datekey = &findkey("DTSTART", \%prop);
		next if not $datekey;

		my $dateval = $prop{$datekey};
		die if $dateval !~ /^(\d{4})(\d\d)(\d\d)(T(\d\d)(\d\d))?/;
		my ($anio, $mes, $dia) = ($1, $2, $3);
		my ($hora, $min) = $4 ? ($5, $6) : (0, 0);

		my $rrulekey = &findkey("RRULE", \%prop);
		if ($rrulekey)
		{
			my %attrs = &get_attrs($prop{$rrulekey});

			if (exists $attrs{UNTIL})
			{
				die if $attrs{UNTIL} !~ /(\d{4})(\d\d)(\d\d)/;
				my $until = timegm(0,0,0,$3,$2 - 1,$1 - 1900);
				next if $until < $ahora;
			}
			
			if ($attrs{FREQ} eq "YEARLY")
			{
				$anio = (localtime($ahora))[5];
				$anio += 1900;
			}
			elsif ($attrs{FREQ} eq "MONTHLY")
			{
				($mes, $anio) = (localtime($ahora))[4, 5];
				$mes++;
				$anio += 1900;
			}
			elsif ($attrs{FREQ} eq "WEEKLY")
			{
				my $fecha = 
					timegm(0, $min, $hora, $dia, $mes - 1, $anio - 1900);
				my ($diasem) = (localtime($fecha))[6];
				$fecha = $ahora + $min_diff;
				while ($fecha < $ahora + $max_diff)				
				{
					last if (localtime($fecha))[6] == $diasem;
					$fecha += 24* 3600; # 1 dia
				}
				while ((localtime($fecha))[6] != $diasem)
				{
					$fecha += 24* 3600; # 1 dia
				}
				($dia, $mes, $anio) = (localtime($fecha))[3, 4, 5];
				$mes++;
				$anio += 1900;
			}
			elsif ($attrs{FREQ} eq "DAILY")
			{
				($dia, $mes, $anio) = (localtime($ahora))[3, 4, 5];
				$mes++;
				$anio += 1900;
			}
			else { die }
		}
		
		my $fecha = timegm(0, $min, $hora, $dia, $mes - 1, $anio - 1900);
		my $diff = $fecha - $ahora;
		next if ($diff < $min_diff or $diff > $max_diff);

		$citas{"$fecha"} = &weekday($fecha, $ahora).
		    (($min or $hora) ? " $hora:$min" : "").
			"<hspace -1/>".$prop{SUMMARY}."\n";
	}
	
	close $fh;
}

sub parse_ab
{
	dbmopen my %ab, AB_FILE, 0444 or die AB_FILE.": $!";

	while (my ($k, $v) = each %ab)
	{
		next if ($v !~ /.*:.*/);
		my @lines = split /\r\n/, $v;
		my ($name, $bday);
		for (@lines)
		{
			chomp;
			if (/^FN:(.*)/) { $name = $1; }
			elsif (/^BDAY:(\d\d\d\d)-(\d\d)-(\d\d)/)
			{
				my $anio = (localtime($ahora))[5];
				$anio += 1900;
				$bday = timegm(0, 0, 0, $3, $2 - 1, $anio);
			}
		}
		if ($name and $bday)
		{
			my $diff = $bday - $ahora;
			next if ($diff < $min_diff or $diff > $max_diff);

			$citas{"$bday"} = &weekday($bday, $ahora)."<hspace -1/>".
			                  $name."'s birthday". "\n";
		}
	}

	dbmclose %ab;
}

&parse_cal;
&parse_ab;

print "<font 'Vera/14'/>";
print "<color 'FF0000'/>";
print "Agenda<hspace -1/>".ucfirst strftime("%a %m/%e", localtime $ahora)."\n";
print "<hline 300/>\n";
print "<font 'Vera/10'/>";
foreach (sort keys %citas)
{
	print $citas{$_};
}

