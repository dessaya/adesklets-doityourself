#! /usr/bin/env perl
#
# This is a back-end script for doityourself. It displays the first three
# processes in CPU usage order.
#
#------------------------------------------------------------------------------

if ($ARGV[0] eq 'init')
{
	print "<font 'VeraMono/10'/>";
	print "<line.stipple 3/>";
	exit 0;
}

$out = `ps axo comm,pcpu --sort -pcpu | head -4`;
@lines = split /\n/, $out;

print "<color 'FFFFFF'/>";
print $lines[0]."\n";
print "<hline -1/><color 'B0B0B0'/>\n";

foreach (@lines[1 .. 3])
{
	print $_, "\n";
}

