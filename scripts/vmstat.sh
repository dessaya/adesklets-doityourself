#!/bin/bash
#
# This is a back-end script for doityourself. It simply displays the output 
# of the vmstat(8) command.

# Exit immediately if any command fails:
set -e

case $1 in
	init)
		echo "<font 'VeraMono/10'/>"
		echo "<background.color None/>"
		;;
	test | click)
		# Remove the 'tail' command if you want to see the headers:
		vmstat -n 1 2 | tail -n 1
		;;
	*)
		echo "Usage: $0 [ init | test | click ] test-value"
esac

