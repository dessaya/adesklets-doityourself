#!/bin/bash
#
# This is a back-end script for doityourself. It shows the mounted filesystems
# and their respective used space, using bar meters.
#
#------------------------------------------------------------------------------

# Exit immediately if any command fails:
set -e

case $1 in
	init)
		echo "<background.border.color 'FFFFFF'/>"
		echo "<test_delay 10/>"
		echo "<color 'A0A0A0'/>"
		echo "<bar_meter.color 'FF000080'/>"
		echo "<bar_meter.border.color 'FFFFFF'/>"
		;;
	test | click)
		df -lx tmpfs | awk ' BEGIN { OFS="" }
			{ 
				if (NR > 1) 
				{
					print $6, "<hspace -1/><bar_meter 50 10 ", $3, " ", $2, "/>"
				}
			}'
		;;

	*)
		echo "Usage: $0 [ init| test | click ] test-value"
esac


