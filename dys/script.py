import adesklets
from os.path import join

# popen2.Popen3 is buggy when used more than once. Use a custom-made Popen3:
from Popen3 import Popen3

from dys.context import context
from dys.config  import config
from dys.markups import markups
from widgets import *

import re

# Context variables used in this module:
context.register('window.padding', 5)
context.register('window.never_shrink', True)
context.register('background.color', '1E1E1E64')
context.register('background.border.color', None)
context.register('background.border.padding', 0)
context.register('background.image', None)
context.register('error.icon', ['error.png', 48, 48])
context.register('error.color', 'FF0000FF')
context.register('error.font', 'Vera/10')
context.register('error', None)
context.register('test_value', None)
context.register('debug', False)
context.register('id', None)
context.register('autoclear', True)
context.register('autorender', True)

class WidgetTree:
	"""
	A set of widgets, organized in a tree, with methods for starting/ending
	rows/columns and finding widgets by id.
	"""
	def __init__(self):
		# A stack of widgets to keep track of the last visited nodes of the
		# tree while parsing:
		self.stack = []

		# A dictionary of defined widgets, grouped by type:
		self.widgets = {
		}

		# The root widget:
		root = VGroupWidget()
		self.register(root)
		self.stack.append(root)

	def register(self, widget):
		"""
		Assign a unique id and add to self.widgets:
		"""
		if not self.widgets.has_key(widget.id_base):
			self.widgets[widget.id_base] = []
		# Assign context id if defined, and if it is not a HGroupWidget:
		if context['id'] and not isinstance(widget, HGroupWidget):
			widget.id = context['id']
			context['id'] = None
		else:
			# Assign a default id:
			widget.id = widget.id_base + \
						str(len(self.widgets[widget.id_base]))
		self.widgets[widget.id_base].append(widget)

	def add_widget(self, widget):
		"Add a normal widget (not a GroupWidget) to the last group in the tree."
		if not isinstance(self.stack[-1], HGroupWidget):
			self.start_line()
		self.stack[-1].add(widget)
		self.register(widget)

	def get_widget(self, id):
		"""
		Returns the widget with the identifier 'id'.
		"""
		for group in self.widgets.itervalues():
			for widget in group:
				if widget.id == id: return widget
		raise 'Widget not found: ' + id

	def start_col(self, widget):
		"Push a new column into the stack"
		if not isinstance(self.stack[-1], HGroupWidget):
			self.start_line()
		self.stack[-1].add(widget)
		self.register(widget)
		self.stack.append(widget)

	def finish_col(self):
		"Pop the current column out of the stack"
		if isinstance(self.stack[-1], HGroupWidget):
			self.finish_line()
		self.stack.pop()

	def start_line(self):
		"Push a new line into the stack"
		if not isinstance(self.stack[-1], VGroupWidget):
			self.start_col(VGroupWidget())
		widget = HGroupWidget()
		self.stack[-1].add(widget)
		self.register(widget)
		self.stack.append(widget)

	def finish_line(self):
		"Pop the current line out of the stack"
		if isinstance(self.stack[-1], VGroupWidget):
			self.finish_col()
		self.stack.pop()

	def get_current_line(self):
		if isinstance(self.stack[-1], HGroupWidget):
			return self.stack[-1]
		return None


class Script:
	"""
	This class provides methods for calling the script and parsing its output.
	The widgets are arranged internally in a tree. The root of the tree is
	a VGroupWidget which contains at least one HGroupWidget.
	"""

	# Some precompiled regexps to parse markups:
	input_splitter_re = re.compile('(<[^< ].*?/>|\\\\(?:\n|$)|\n)')
	markup_parser_re = re.compile('<(\S+)\s*(.*)/>')
	setattr_re = re.compile('^\s*(\S+)\s+(\S+)\s+(.+)')

	def _call(self, arglist):
		"""
		Call the back-end script, with the list of arguments taken from 'args'.
		"""
		script_bin = join(config.basedir, 'scripts', config['script'])
		args = ' '.join(['"' + arg + '"' for arg in arglist])
		command = script_bin + ' ' + args

		if context['debug']:
			print '=' * 75
			print '****** DEBUG -- Calling: ' + command

		p = Popen3(command, capturestderr = True)
		if p.retcode:
			# Script failed; capture stderr:
			context['error'] = p.err
		else:
			context['error'] = None

		if arglist and arglist[0] == 'init':
			self.parse(p.out)
		else:
			if context['autoclear'] and not context['error']:
				self.clear()
			self.parse(p.out)
			if context['autorender'] and not context['error']:
				self.render(self.widget_tree)

	def __init__(self, id):
		"""
		call the back-end script with the 'init' argument.
		"""
		self.last_background = None
		self.clear()
		if not config['rawmode']:
			args = ['init', str(id)]
			if context['test_value'] != None:
				args += [`context['test_value']`]
			self._call(args)

	def test(self):
		"""
		call the back-end script with the 'test' argument.
		"""
		args = []
		if not config['rawmode']:
			args += ['test']
			if context['test_value'] != None:
				args += [`context['test_value']`]
		self._call(args)

	def click(self, widget_id = None):
		"""
		call the back-end script with the 'click' or 'click_widget' argument.
		"""
		args = []
		if not config['rawmode']:
			if widget_id:
				args += ['click_widget', widget_id]
			else:
				args += ['click']
			if context['test_value'] != None:
				args += [`context['test_value']`]
		self._call(args)

	def parse_markup(self, markup):
		"""
		Parse the markup, and add widget(s) to the tree, if appropriate.
		"""
		if not markup: return
		if markup[0] == '\n':
			# We must finish the current line if it is an empty line, or a line
			# with at least one widget:
			if markup[1:] == 'empty' or self.widget_tree.get_current_line() and\
			   self.widget_tree.get_current_line().widgets:
				if markup[1:] == 'empty':
					# Empty line: add an empty text widget
					self.widget_tree.add_widget(TextWidget(''))
				# Finish the current line:
				self.widget_tree.finish_line()
		else:
			m = self.markup_parser_re.match(markup)
			if m:
				# This is a formatting markup, i.e. '<blah blah/>'
				name = m.group(1)
				args = m.group(2)
				if name == 'render':
					if not context['error']: self.render(self.widget_tree)
				elif name == 'clear':
					self.clear()
				elif name == 'setattr':
					n = self.setattr_re.match(args)
					widget = self.widget_tree.get_widget(n.group(1)[1:-1])
					attr = n.group(2)[1:-1]
					# Raise an error if the widget has no attribute 'attr':
					getattr(widget, attr)
					setattr(widget, attr, eval(n.group(3)))
				# Try with one of the registered markups:
				elif markups.has_key(name):
					markups[name](name, args, self.widget_tree)
				# Try with one of the context variables:
				elif context.has_key(name):
					context[name] = eval(args)
				else:
					# Markup not recognized: create a TextWidget.
					self.widget_tree.add_widget(TextWidget(markup))
			else:
				# This is a string of text (not a markup):
				self.widget_tree.add_widget(TextWidget(markup))

	def clear(self):
		"""
		Start a new set of widgets, and forget the previous one:
		"""
		self.widget_tree = WidgetTree()

	def parse(self, input):
		"""
		Parse the script output, create the widgets and compute the total size
		needed.
		"""
		if not context['error']:
			if config['rawmode']:
				# Ignore markups:
				self.clear()
				for line in input.expandtabs().splitlines():
					self.widget_tree.start_line()
					self.widget_tree.add_widget(TextWidget(line))
					self.widget_tree.finish_line()
			else:
				if context['autoclear']: self.clear()
				input = input.expandtabs()
				# Split line into recognized markups and ignore
				# escaped linebreaks:
				markups = [m for m in self.input_splitter_re.split(input) \
							 if m and m != '\\\n']
				# Mark empty lines:
				for i, m in enumerate(markups):
					if m == '\n':
						if i == 0 or markups[i - 1] == '\n':
							markups[i] += 'empty'
				# Parse each markup separately:
				for m in markups:
					self.parse_markup(m)

		if context['error']:
			# Show error message
			#
			error_tree = WidgetTree()
			# Show the error icon:
			if context['error.icon']:
				error_tree.start_col(VGroupWidget())
				error_tree.add_widget(ImageWidget(
				                ' '.join([`i` for i in context['error.icon']])))
				error_tree.finish_col()
				error_tree.start_col(VGroupWidget())
			# Show the error message:
			for line in context['error'].expandtabs().splitlines():
				error_tree.start_line()
				error_tree.add_widget(TextWidget(line,
				                           color = context['error.color'],
				                           font = context['error.font']))
				error_tree.finish_line()
			self.render(error_tree)

		# Output debug info:
		#
		if context['debug']:
			print '****** DEBUG ** Script output:'
			print input
			print '****** DEBUG ** Widgets tree:'
			print `self.widget_tree.stack[0]`
			print

	def render_background(self):
		# Get current window size:
		adesklets.context_set_image(1)
		adesklets.context_set_blend(True)
		w = adesklets.image_get_width()
		h = adesklets.image_get_height()

		if context['background.image']:
			# Get background image size:
			bg_image = context.set_image(context['background.image'])
			image_w = adesklets.image_get_width()
			image_h = adesklets.image_get_height()

			# Fill with the background image
			adesklets.context_set_image(1)
			adesklets.blend_image_onto_image(
			    context.load_image(context['background.image']), 1,
			    0, 0, image_w, image_h, 0, 0, image_w, image_h)
		else:
			# Fill with the background color
			if context['background.color']:
				context.set_color(context['background.color'])
				adesklets.image_fill_rectangle(0, 0, w, h)

			# Draw the border
			if context['background.border.color']:
				context.set_color(context['background.border.color'])
				p = context['background.border.padding']
				adesklets.image_draw_rectangle(p, p, w - 2 * p, h - 2 * p)

		# Remember last rendered settings:
		self.last_background = (
			context['background.image'],
			context['background.color'],
			context['background.border.color'])

	def render(self, widget_tree):
		"""
		This is the main drawing routine
		"""
		# Get the root widget for rendering
		#
		root_widget = widget_tree.stack[0]

		# Calculate root widget dimensions
		#
		root_widget.compute_size()

		# Compute the window dimensions
		#
		w, h = 0, 0

		# Offset from border:
		padding = context['window.padding']
		if context['background.border.color'] and \
		   not context['background.image']:
			padding += context['background.border.padding']

		w = root_widget.width  + 2 * padding
		h = root_widget.height + 2 * padding

		# Get background image size, and resize if necessary:
		if context['background.image']:
			bg_image = context.set_image(context['background.image'])
			w = max(adesklets.image_get_width(), w)
			h = max(adesklets.image_get_height(), h)

		adesklets.context_set_image(0)
		if context['window.never_shrink']:
			w = max(adesklets.image_get_width(), w)
			h = max(adesklets.image_get_height(), h)
			# Resize root widget:
			root_widget.width  = w - 2 * padding
			root_widget.height = h - 2 * padding

		# Resize window if necessary:
		if w != adesklets.image_get_width() or h!= adesklets.image_get_height():
			adesklets.context_set_color(0, 0, 0, 0)
			adesklets.image_fill_rectangle(0, 0,
			        adesklets.image_get_width(), adesklets.image_get_height())
			adesklets.window_resize(w, h)

		# Render background, if necessary:
		#
		if self.last_background != (
		   context['background.image'],
		   context['background.color'],
		   context['background.border.color']):
			self.render_background()

		# Render widgets:
		#

		# Create a buffer:
		buffer_id = context.create_image(max(w, 1), max(h, 1))
		adesklets.context_set_image(buffer_id)
		adesklets.context_set_blend(False)
		adesklets.context_set_color(0, 0, 0, 0)
		adesklets.image_fill_rectangle(0, 0, w, h)
		adesklets.context_set_blend(True)

		# Traverse the tree and draw the widgets:
		root_widget.render(x = padding, y = padding)

		# Blend buffer into foreground:
		adesklets.context_set_image(0)
		adesklets.context_set_blend(False)
		adesklets.blend_image_onto_image(buffer_id, 1,
		                                 0, 0, w, h,
		                                 0, 0, w, h)
		context.free_image(buffer_id)

# vim: ts=4: sw=4
