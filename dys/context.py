import adesklets
from os.path import join
from dys.config import config

class Context(dict):
	"""
	This class contains the context variables, which define the state of the
	parsing and rendering engine. The actual variables are added by other
	modules.
	"""
	def __init__(self):
		# 'Private' context variables: these may not be modified by the
		# back-end script.
		#
		dict.__init__(self)
		self._loaded_fonts = []
		self._loaded_images = []
		self._color_mod = None

	def __setitem__(self, key, value):
		"""
		Overrided in order to raise an error if the key does not exist.
		"""
		if self.has_key(key):
			dict.__setitem__(self,key,value)
		else:
			raise key + ' key does not already exist.\n' + \
			      'Use register() to register new context variables.'

	def register(self, key, value):
		"""
		Register a new context variable.
		"""
		if not self.has_key(key):
			dict.__setitem__(self,key,value)
		else:
			raise key + ' key already exists.\n'

	def ready(self):
		"""
		Called after all modules have been loaded.
		"""
		# Find settings for context variables in config:
		for key, value in config.iteritems():
			if config.cfg_default.has_key(key): continue
			if self.has_key(key):
				self[key] = value

		# Color modifier for highlighting widgets:
		self._color_mod = adesklets.create_color_modifier()
		adesklets.context_set_color_modifier(self._color_mod)
		adesklets.modify_color_modifier_gamma(1.3)
		adesklets.context_set_color_modifier(None)

		# List of already loaded images:
		self._loaded_images = ['_foreground', '_background']

	def create_image(self, w, h, name = '_unnamed'):
		"""
		Wrapper for adesklets.create_image() which remembers images already
		loaded.
		"""
		id = adesklets.create_image(w, h)
		if id == len(self._loaded_images):
			self._loaded_images.append(name)
		else:
			raise 'Images list displaced. id('+`id`+'), images(' + \
			      `self._loaded_images` + ')'
		return id

	def load_image(self, filename):
		"""
		Wrapper for adesklets.load_image() which remembers images already
		loaded.
		"""
		if filename in self._loaded_images:
			return self._loaded_images.index(filename)
		else:
			id = adesklets.load_image(join(config.basedir, 'icons', filename))
			if id == len(self._loaded_images):
				self._loaded_images.append(filename)
			else:
				raise 'Images list displaced. id('+`id`+'), images(' + \
				      `self._loaded_images` + ')'
			return id

	def set_image(self, filename):
		"""
		Wrapper for adesklets.context_set_image() which remembers images
		already loaded.
		"""
		id = self.load_image(filename)
		adesklets.context_set_image(id)
		return id

	def free_image(self, id):
		"""
		Wrapper for adesklets.free_image().
		"""
		adesklets.free_image(id)
		del self._loaded_images[id]
		return id

	def load_font(self, fontname):
		"""
		Wrapper for adesklets.load_fonts() which remembers fonts already loaded.
		"""
		if fontname in self._loaded_fonts:
			return self._loaded_fonts.index(fontname)
		else:
			id = adesklets.load_font(fontname)
			if id == len(self._loaded_fonts):
				self._loaded_fonts.append(fontname)
			else:
				raise 'Fonts list displaced. id('+`id`+'), fonts(' + \
				      `self._loaded_fonts` + ')'
			return id

	def set_font(self, fontname):
		"""
		Wrapper for adesklets.context_set_font() which remembers fonts
		already loaded.
		"""
		id = self.load_font(fontname)
		adesklets.context_set_font(id)
		return id

	def set_color(self, string):
		"""
		Wrapper for adesklets.context_set_color(), which uses a color string.
		"""
		list = [eval('0x%s' % string[i*2:i*2+2]) for i in range(len(string)/2)]
		if len(list) == 3:
			list += [ 255 ]
		adesklets.context_set_color(*list)

# Global context instance:
context = Context()

# vim: ts=4: sw=4
