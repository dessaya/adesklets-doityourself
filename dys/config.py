import adesklets
from os.path import join, dirname, normpath

class Config(adesklets.ConfigFile):
	"""
	This is doityourself.py desklet configuration file.

	There are two modes of operation, defined by the 'rawmode' config
	variable: normal and raw.

	In normal mode (the default), the back-end script is called using the
	agruments defined in the doityourself script interface, and its output
	is parsed in order to find markups.

	In raw mode, only the arguments specified in 'script' are passed, and the
	output is treated as raw text. Use raw mode if you don't want to write
	a script, and set 'script' to any command that writes some output to
	stderr. For example:

		id0 = { 'rawmode': True, 'script': '/bin/date' }

	Additionally, in raw mode you may want to customize some context
	variables: you can do so here. Example:

		id0 = {
			'rawmode': True,
			'script': '/bin/date "+%D"',
			'color': '00FF00',
			'background.color': None
		}

	If 'script' is defined as a relative path, it will be assumed to be
	located in the scripts/ directory.
	"""
	cfg_default = {
		'script' : 'example.sh',
		'rawmode' : False,
	}

	def __init__(self):
		self.basedir = normpath(join(dirname(__file__), '..'))
		if len(self.basedir) == 0:
			self.basedir = '.'
		adesklets.ConfigFile.__init__(self, adesklets.get_id(),
		                              join(self.basedir,'config.txt'))

# Global config instance:
config = Config()

# vim: ts=4: sw=4
