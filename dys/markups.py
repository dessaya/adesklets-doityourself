class Markups(dict):
	"""
	This class contains the markups registered by modules.
	"""
	def __setitem__(self, key, value):
		"""
		Overrided in order to raise an error if the key does not exist.
		"""
		if self.has_key(key):
			dict.__setitem__(self, key, value)
		else:
			raise key + ' key does not already exist.\n' + \
			      'Use register() to register new markups.'

	def register(self, markup, callback):
		"""
		Register a new markup.
		"""
		if not self.has_key(markup):
			dict.__setitem__(self, markup, callback)
		else:
			raise markup + ' markup already exists.\n'


# Global Markups instance:
markups = Markups()

# vim: ts=4: sw=4
