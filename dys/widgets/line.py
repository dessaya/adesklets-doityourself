import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget

# Context variables related to lines:
context.register('line.stipple', 0)

# Markup for creating a line:
markups.register(
	'hline',
	lambda name, args, widget_tree:
		widget_tree.add_widget(LineWidget('hline', args))
)
markups.register(
	'vline',
	lambda name, args, widget_tree:
		widget_tree.add_widget(LineWidget('vline', args))
)

class LineWidget(Widget):
	"""
	A horizontal or vertical line widget.
	"""
	def __init__(self, type, args):
		self.id_base = type
		Widget.__init__(self)
		self.stipple = context['line.stipple']

		try:
			value = int(args.strip())
		except:
			raise 'Invalid ' + self.id_base + ' attributes: ' + args

		if type == 'hline':
			self.width = value
			if value < 0:
				self.autowidth = True
			self.height = 1
		else:
			self.height = value
			if value < 0:
				self.autoheight = True
			self.width = 1

	def __repr__(self):
		return '<' + self.id + ' ' + `self.width` + '>'

	def _draw_solid(self, x, y, length):
		if self.id_base == 'hline':
			adesklets.image_draw_line(x, y, x + length - 1, y, 0)
		else:
			adesklets.image_draw_line(x, y, x, y + length - 1, 0)

	def _draw_stippled(self, x, y, length, steps):
		if not steps % 2:
			steps -= 1
		if self.id_base == 'hline':
			step_x = x + (length - steps * self.stipple) / 2
			for i in range(steps):
				if not i % 2:
					adesklets.image_draw_line(step_x, y,
											  step_x + self.stipple - 1, y,
											  0)
				step_x += self.stipple
		else:
			step_y = y + (length - steps * self.stipple) / 2
			for i in range(steps):
				if not i % 2:
					adesklets.image_draw_line(x, step_y,
											  x, step_y + self.stipple - 1,
											  0)
				step_y += self.stipple

	def render(self, x, y):
		Widget.render(self, x, y)
		length = (self.width, self.height)[self.id_base == 'vline']
		if self.stipple:
			steps = length / self.stipple
			if steps:
				self._draw_stippled(x, y, length, steps)
			else:
				self._draw_solid(x, y, length)
		else:
			self._draw_solid(x, y, length)

# vim: ts=4: sw=4
