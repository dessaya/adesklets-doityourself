import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget

from math import log

# Context variables related to bar meters:
context.register('bar_meter.padding', 1)
context.register('bar_meter.color', None)
context.register('bar_meter.border.color', None)
context.register('bar_meter.orientation', 'LR')
context.register('bar_meter.logarithmic', False)

# Markup for creating a bar meter:
markups.register(
	'bar_meter',
	lambda name, args, widget_tree:
		widget_tree.add_widget(BarMeterWidget(args))
)

class BarMeterWidget(Widget):
	"""
	A bar meter widget.
	"""
	id_base = 'bar_meter'

	def __init__(self, args):
		Widget.__init__(self)
		# Get all the attributes:
		try:
			arglist = args.split()
			self.width = eval(arglist[0])
			if self.width < 0:
				self.autowidth = True
			if self.height < 0:
				self.autoheight = True
			self.height = eval(arglist[1])
			# Get value and max:
			if len(arglist) >= 3:
				self.value = eval(arglist[2])
			else:
				self.value = 0
			if len(arglist) >= 4:
				self.max = eval(arglist[3])
			else:
				self.max = 100
		except:
			raise 'Invalid bar meter attributes: ' + args

		self.padding = context['bar_meter.padding']
		self.color = context['bar_meter.color'] or self.color
		self.border_color = context['bar_meter.border.color'] or \
		                    context['color']
		self.orientation = context['bar_meter.orientation']
		self.logarithmic = context['bar_meter.logarithmic']

	def __repr__(self):
		return '<%s %d %d>' % (self.id, self.width, self.height)

	def render(self, x, y):
		Widget.render(self, x, y)

		# Draw the meter:
		if self.logarithmic:
			value = log(self.value + 1)
			val_max = log(self.max + 1)
		else:
			value   = self.value
			val_max = self.max
		p = self.padding + 1
		if self.orientation == 'BT' or self.orientation == 'TB':
			max_length = self.height - 2 * p
		else:
			max_length = self.width - 2 * p
		length = int(value * max_length / val_max)
		length = max(0, min(length, max_length))
		if length:
			if self.orientation == 'LR': # Left-Right
				adesklets.image_fill_rectangle(
				                          x + p, y + p,
				                          length, self.height - 2 * p)
			elif self.orientation == 'RL': # Right-Left
				adesklets.image_fill_rectangle(
				                          x + self.width - p - length, y + p,
				                          length, self.height - 2 * p)
			elif self.orientation == 'TB': # Top-Bottom
				adesklets.image_fill_rectangle(
				                          x + p, y + p,
				                          self.width - 2 * p, length)
			else: # Bottom-Top
				adesklets.image_fill_rectangle(
				                          x + p, y + self.height - p - length,
				                          self.width - 2 * p, length)

		# Draw the border:
		context.set_color(self.border_color)
		adesklets.image_draw_rectangle(x, y, self.width, self.height)

# vim: ts=4: sw=4
