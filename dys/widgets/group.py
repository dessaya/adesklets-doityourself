import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget

# Context variables related to GroupWidgets:
context.register('valign', 'center')
context.register('halign', 'left')
context.register('vpadding', 3)
context.register('hpadding', 3)

# Markup for starting/ending columns:
markups.register(
	'col', # Begin a column of widgets
	lambda name, args, widget_tree:
		widget_tree.start_col(VGroupWidget())
)
markups.register(
	'endcol', # End a column
	lambda name, args, widget_tree:
		widget_tree.finish_col()
)

class GroupWidget(Widget):
	"""
	Widgets container. This is the base class for HGroupWidget and VGroupWidget.
	"""
	def __init__(self):
		Widget.__init__(self)
		self.widgets = []

	def compute_size(self):
		self._autowidth_count = 0
		self._autoheight_count = 0
		self.autowidth = False
		self.autoheight = False
		self.clickable = False
		self.pointable = False
		for w in self.widgets:
			w.compute_size()
			if w.autowidth:
				self._autowidth_count += 1
				self.autowidth = True
			if w.autoheight:
				self._autoheight_count += 1
				self.autoheight = True
			if w.clickable: self.clickable = True
			if w.pointable: self.pointable = True

	def add(self, widget):
		self.widgets.append(widget)

	def autosize(self, widget, spare_width, spare_height):
		if widget.autowidth:
			widget.width = widget.min_width + spare_width
		if widget.autoheight:
			widget.height = widget.min_height + spare_height

	def render(self, x, y):
		Widget.render(self, x, y)

	# Event callbacks:
	#
	def click(self, x, y):
		for w in self.widgets:
			if w.clickable and \
			   w.x <= x < w.x + w.width and w.y <= y < w.y + w.height:
				return w.click(x, y)
		return None

	def motion_notify(self, x, y):
		Widget.motion_notify(self, x, y)
		for w in self.widgets:
			if w.pointable:
				if w.x <= x < w.x + w.width and w.y <= y < w.y + w.height:
					w.motion_notify(x, y)
				elif w.hovering:
					w.leave_notify()

	def leave_notify(self):
		Widget.leave_notify(self)
		for w in self.widgets:
			if w.pointable and w.hovering:
				w.leave_notify()


class HGroupWidget(GroupWidget):
	"""
	A container for widgets arranged horizontally.
	"""
	id_base = 'hgroup'

	def __init__(self):
		GroupWidget.__init__(self)
		self.valign = context['valign']
		self.hpadding = context['hpadding']

	def __repr__(self):
		return '<' + self.id + ' ' + `self.widgets` + '>'

	def compute_size(self):
		if self.widgets:
			GroupWidget.compute_size(self)
			padding = self.hpadding * (len(self.widgets) - 1)
			self.width = sum([w.width for w in self.widgets]) + padding
			self.min_width = sum([w.width for w in self.widgets
			                                  if not w.autowidth]) + \
			                     sum([w.min_width for w in self.widgets
			                                  if w.autowidth]) + \
			                     padding
			self.height = self.min_height= max([w.height for w in self.widgets])

	def render(self, x, y):
		GroupWidget.render(self, x, y)
		if not self.widgets: return
		if self.autowidth:
			free_width = self.width - self.min_width
			spare_width = free_width / self._autowidth_count
		else:
			spare_width = 0
		widget_x = x
		for widget in self.widgets:
			spare_height = self.height - widget.min_height
			self.autosize(widget, spare_width, spare_height)
			widget_y = y
			if self.valign == 'center':
				widget_y += (self.height - widget.height) / 2
			elif self.valign == 'bottom':
				widget_y += self.height - widget.height
			widget.render(widget_x, widget_y)
			widget_x += widget.width + self.hpadding


class VGroupWidget(GroupWidget):
	"""
	A container for widgets arranged vertically.
	"""
	id_base = 'vgroup'

	def __init__(self):
		GroupWidget.__init__(self)
		self.halign = context['halign']
		self.vpadding = context['vpadding']

	def __repr__(self):
		return '<' + self.id + ' ' + `self.widgets` + '>'

	def compute_size(self):
		if self.widgets:
			GroupWidget.compute_size(self)
			padding = self.vpadding * (len(self.widgets) - 1)
			self.height = sum([w.height for w in self.widgets]) + padding
			self.min_height = sum([w.height for w in self.widgets
			                                    if not w.autoheight]) + \
			                     sum([w.min_height for w in self.widgets
			                                       if w.autoheight]) + \
			                     padding
			self.width = self.min_width = max([w.width for w in self.widgets])

	def render(self, x, y):
		GroupWidget.render(self, x, y)
		if not self.widgets: return
		if self.autoheight:
			free_height = self.height - self.min_height
			spare_height = free_height / self._autoheight_count
		else:
			spare_height = 0
		widget_y = y
		for widget in self.widgets:
			spare_width = self.width - widget.min_width
			self.autosize(widget, spare_width, spare_height)
			widget_x = x
			if self.halign == 'center':
				widget_x += (self.width - widget.width) / 2
			elif self.halign == 'right':
				widget_x += self.width - widget.width
			widget.render(widget_x, widget_y)
			widget_y += widget.height + self.vpadding

# vim: ts=4: sw=4
