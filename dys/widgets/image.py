import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget

import re

string_re = re.compile('\'.*?\'|".*?"')
number_re = re.compile('\\b-?(?:(?:\d+\.\d*)|(?:\d*\.\d+)|(?:\d+))\\b')

# Markup for creating an image:
markups.register(
	'image',
	lambda name, args, widget_tree:
		widget_tree.add_widget(ImageWidget(args))
)

class ImageWidget(Widget):
	"""
	An image widget.
	"""
	id_base = 'image'

	def __init__(self, args):
		Widget.__init__(self)
		# Get the attributes:
		try:
			match = string_re.search(args)
			filename = args[match.start() : match.end()]
			dim_list = number_re.findall(args[match.end() :])

			self.filename = eval(filename)
			context.set_image(self.filename)
			self._image_width = adesklets.image_get_width()
			self._image_height = adesklets.image_get_height()
			self.width = self._image_width
			self.height = self._image_height

			if dim_list:
				[ width, height ] = dim_list
				self.width = int(width)
				self.height = int(height)
		except adesklets.error_handler.ADESKLETSError:
			raise
		except:
			raise 'Invalid image attributes: ' + args

	def __repr__(self):
		return '<' + self.id + ' ' + `self.filename` + '>'

	def render(self, x, y):
		Widget.render(self, x, y)
		adesklets.blend_image_onto_image(context.load_image(self.filename), 1,
										 0, 0,
		                                 self._image_width, self._image_height,
										 x, y,
										 self.width, self.height)

# vim: ts=4: sw=4
