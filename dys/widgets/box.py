import adesklets
from dys.context import context
from dys.markups import markups
from group import VGroupWidget

# Context variables related to GroupWidgets:
context.register('box.padding', 5)

# Markups for creating/ending a box:
markups.register(
	'box',
	lambda name, args, widget_tree:
		widget_tree.start_col(BoxWidget(args))
)
markups.register(
	'endbox',
	lambda name, args, widget_tree:
		widget_tree.finish_col()
)

class BoxWidget(VGroupWidget):
	"""
	A box with border and fill color that acts like a VGroupWidget.
	"""
	id_base = 'box'

	def __init__(self, args):
		VGroupWidget.__init__(self)
		try:
			[ self.color, self.border_color ] = [ eval(c) \
			                           for c in args.split() ]
			self.padding = context['box.padding']
		except:
			raise 'Invalid box attributes: ' + args

	def __repr__(self):
		return '<' + self.id + ' ' + `self.widgets` + '>'

	def compute_size(self):
		VGroupWidget.compute_size(self)
		self.width += 2 * self.padding
		self.height += 2 * self.padding
		self.min_width += 2 * self.padding
		self.min_height += 2 * self.padding

	def render(self, x, y):
		# Fill with the background color
		if self.color:
			context.set_color(self.color)
			adesklets.image_fill_rectangle(x, y, self.width, self.height)

		# Draw the border
		if self.border_color:
			context.set_color(self.border_color)
			adesklets.image_draw_rectangle(x, y, self.width, self.height)

		# Render the contained widgets:
		self.width -= 2 * self.padding
		self.height -= 2 * self.padding
		self.min_width -= 2 * self.padding
		self.min_height -= 2 * self.padding
		VGroupWidget.render(self, x + self.padding, y + self.padding)
		self.width += 2 * self.padding
		self.height += 2 * self.padding
		self.min_width += 2 * self.padding
		self.min_height += 2 * self.padding

# vim: ts=4: sw=4
