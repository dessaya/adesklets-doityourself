import adesklets
from dys.context import context
from widget import Widget

import re

class TextWidget(Widget):
	"""
	A widget containing some text.
	"""
	spaces_re = re.compile('^(\s*)(.*?)(\s*)$')
	id_base = 'text'

	def __init__(self, text, font = None, color = None):
		Widget.__init__(self, color = color)
		self.font = font or context['font']
		self.text = text

	def compute_size(self):
		self._initial_spaces_width = 0
		self._text_width = 0
		self._trailing_spaces_width = 0

		# Compute widget dimensions:
		self._font_id = context.set_font(self.font)
		self.height = adesklets.get_text_advance('-')[1]
		m = self.spaces_re.search(self.text)
		if m.group(1):
			self._initial_spaces_width = \
			       adesklets.get_text_advance(m.group(1).replace(' ', '-'))[0]
		if m.group(2):
			(self._text_width, self.height) = \
			                            adesklets.get_text_advance(m.group(2))
		if m.group(3):
			self._trailing_spaces_width = \
			       adesklets.get_text_advance(m.group(3).replace(' ', '-'))[0]
		self.width = self._initial_spaces_width + self._text_width + \
		             self._trailing_spaces_width

	def __repr__(self):
		return '<' + self.id + ' ' + `self.text` + '>'

	def render(self, x, y):
		Widget.render(self, x, y)
		if self.text:
			adesklets.context_set_font(self._font_id)
			adesklets.text_draw(x + self._initial_spaces_width, y, self.text.strip())

# vim: ts=4: sw=4
