import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget

# Markups for creating a space:
markups.register(
	'hspace',
	lambda name, args, widget_tree:
		widget_tree.add_widget(SpaceWidget('hspace', args))
)
markups.register(
	'vspace',
	lambda name, args, widget_tree:
		widget_tree.add_widget(SpaceWidget('vspace', args))
)

class SpaceWidget(Widget):
	"""
	An empty widget.
	"""
	def __init__(self, type, args):
		self.id_base = type
		Widget.__init__(self)

		try:
			value = int(args.strip())
		except:
			raise 'Invalid ' + type + ' attributes: ' + args

		if type == 'hspace':
			self.width = value
			if value < 0:
				self.autowidth = True
			self.height = 0
		else:
			self.height = value
			if value < 0:
				self.autoheight = True
			self.width = 0

	def __repr__(self):
		return '<' + self.id + ' ' + \
		       `(self.width, self.height)[self.id_base == 'vspace']` + '>'

	def render(self, x, y):
		Widget.render(self, x, y)

# vim: ts=4: sw=4
