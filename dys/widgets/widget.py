import adesklets
from dys.context import context

# Context variables related to all widgets:
context.register('widget.min_size', 5)
context.register('color', 'FFFFFFFF')
context.register('font', 'Vera/10')

class Widget:
	"""
	Base class for all widgets.
	"""
	# Set to True in derived class to receive click() events:
	clickable = False
	# Set to True in derived class to receive motion_notify() and
	# leave_notify() events:
	pointable = False

	def __init__(self, color = None):
		self.width = 0
		self.height = 0
		self.color = color or context['color']
		self.autowidth = False
		self.autoheight = False
		self.min_width = 0
		self.min_height = 0
		self.hovering = False
		self.id = self.id_base
		self.x = -1
		self.y = -1

	def compute_size(self):
		if self.autowidth:
			self.width = self.min_width = context['widget.min_size']
		if self.autoheight:
			self.height = self.min_height = context['widget.min_size']

	def render(self, x, y):
		self.x = x
		self.y = y
		if self.color:
			context.set_color(self.color)

	# Overridable event callbacks:
	#
	def click(self, x, y):
		return self.id

	def motion_notify(self, x, y):
		self.hovering = True

	def leave_notify(self):
		self.hovering = False

# vim: ts=4: sw=4
