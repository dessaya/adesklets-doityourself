import adesklets
from dys.context import context
from dys.markups import markups
from widget import Widget
from image import ImageWidget

# Context variables related to buttons:
context.register('button.highlight', True)

# Markup for creating a button:
markups.register(
	'button',
	lambda name, args, widget_tree:
		widget_tree.add_widget(ButtonWidget(args))
)

class ButtonWidget(ImageWidget):
	"""
	An image that can be highlighted when the mouse pointer hovers and can
	react to user clicks.
	"""
	id_base = 'button'
	clickable = True
	pointable = True

	def __init__(self, args):
		try:
			ImageWidget.__init__(self, args)
		except adesklets.error_handler.ADESKLETSError:
			raise
		except:
			raise 'Invalid button attributes: ' + args
		self.highlight = context['button.highlight']

	def __repr__(self):
		return '<' + self.id + ' ' + `self.filename` + '>'

	def redraw(self):
		"""
		Called after the rendering phase, when only this button needs redrawing
		"""
		buffer_id = context.create_image(self.width, self.height)
		adesklets.context_set_image(buffer_id)
		adesklets.context_set_blend(False)
		adesklets.context_set_color(0, 0, 0, 0)
		adesklets.image_fill_rectangle(0, 0, self.width, self.height)

		adesklets.context_set_blend(True)
		if self.hovering and self.highlight:
			adesklets.context_set_color_modifier(context._color_mod)
		x, y = self.x, self.y
		ImageWidget.render(self, 0, 0)
		self.x, self.y = x, y
		adesklets.context_set_color_modifier(None)

		adesklets.context_set_image(0)
		adesklets.context_set_blend(False)
		adesklets.blend_image_onto_image(buffer_id, 1,
		                                 0, 0, self.width, self.height,
										 self.x,self.y, self.width,self.height)
		context.free_image(buffer_id)

	def motion_notify(self, x, y):
		last = self.hovering
		ImageWidget.motion_notify(self, x, y)
		if self.hovering != last:
			self.redraw()

	def leave_notify(self):
		last = self.hovering
		ImageWidget.leave_notify(self)
		if self.hovering != last:
			self.redraw()

	def render(self, x, y):
		ImageWidget.render(self, x, y)

# vim: ts=4: sw=4
