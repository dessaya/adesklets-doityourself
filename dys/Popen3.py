from os import system
from tempfile import NamedTemporaryFile

class Popen3:
	"""
	Replacement for buggy popen2.Popen3, adapted from Python FAQ.
	Executes a command, and captures its output (return code, stdout and
	stderr).
	"""
	def __init__(self, command, input = None, capturestderr = None):
		self.retcode = None
		self.out = ''
		self.err = ''

		outfile = NamedTemporaryFile()
		command = "%s > %s" % (command, outfile.name)
		if input:
			infile = NamedTemporaryFile()
			infile.write(input)
			command = command + " <" + infile.name
		if capturestderr:
			errfile = NamedTemporaryFile()
			command = command + " 2>" + errfile.name

		self.retcode = system(command) >> 8

		self.out = outfile.read()
		if capturestderr:
			self.err = errfile.read()

# vim: ts=4: sw=4
