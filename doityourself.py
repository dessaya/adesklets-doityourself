#! /usr/bin/env python
"""
--------------------------------------------------------------------------------
Copyright (C) 2005  Diego Essaya <dessaya@fi.uba.ar>

Released under the GPL, version 2. Except the artwork, which is released
pursuant to the restrictions imposed by their authors.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--------------------------------------------------------------------------------

doityourself is a generic customizable desklet. See README for details.

--------------------------------------------------------------------------------
"""
import adesklets
from os import getenv, spawnlp, P_NOWAIT
from os.path import join
from dys.context import context
from dys.config import config
from dys.script import Script

# Context variables used in this module:
context.register('test_delay', 5)
context.register('window.click_anywhere', True)

class Events(adesklets.Events_handler):
	"""
	Events driving class.
	"""
	def __init__(self):
		adesklets.add_path_to_font_path('./fonts')
		adesklets.Events_handler.__init__(self)

	def __del__(self):
		adesklets.Events_handler.__del__(self)

	def ready(self):
		"""
		Real initialisation takes place here.
		"""
		# Load default resources
		#
		context.ready()
		self.script = Script(adesklets.get_id())

		# Add menu items
		#
		adesklets.menu_add_separator()
		adesklets.menu_add_item('Update')
		adesklets.menu_add_separator()
		adesklets.menu_add_item('Configure')

		# Set the window properties
		#
		adesklets.window_set_transparency(True)
		adesklets.window_show()

	def alarm(self):
		"""
		Perform the test and refresh the display
		"""
		self.block()
		self.script.test()
		self.unblock()
		return context['test_delay']

	def button_press(self, delayed, x, y, button):
		"""
		Call the script with 'click' or 'click_widget' argument. In raw mode,
		just update.
		"""
		if context['error']: return
		if config['rawmode']:
			self.script.test()
		elif self.script.widget_tree:
			# Ugly hack follows: If this is the first click, a false
			# leave_notify event is raised before.
			# Thus, we call motion_notify() to update widget.hovering:
			self.motion_notify(0, x, y)
			widget_id = self.script.widget_tree.stack[0].click(x, y)
			if not widget_id:
				if context['window.click_anywhere']:
					self.script.click()
			else:
				self.script.click(widget_id)

	def motion_notify(self, delayed, x, y):
		"""
		Determine if the pointer is hovering over any of the widgets
		"""
		if context['error']: return
		if not delayed and not config['rawmode'] and self.script.widget_tree:
			self.script.widget_tree.stack[0].motion_notify(x, y)

	def leave_notify(self, delayed, x, y):
		"""
		Mark all widgets as not hovering
		"""
		if context['error']: return
		if not config['rawmode'] and self.script.widget_tree:
			self.script.widget_tree.stack[0].leave_notify()

	def menu_fire(self, delayed, menu_id, item):
		if item == 'Configure':
			editor = getenv('EDITOR') or 'vi'
			command = 'xterm -e %s %s' % \
			          (editor, join(config.basedir,'config.txt'))
			spawnlp(P_NOWAIT, command.split()[0], *command.split())
		elif item == 'Update':
			self.script.test()

	def background_grab(self, delayed):
		"""
		Update background (image id 1)
		"""
		self.script.render_background()

#-------------------------------------------------------------------------------
# Now, let's start everything
#
Events().pause()

# vim: ts=4: sw=4
